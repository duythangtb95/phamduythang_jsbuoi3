/*bài 1: Tính tiền lương nhân viên
lương 1 ngày: 100.000
cho người dùng nhập vào số ngày
công thức tính lương: lương 1 ngày * số ngày làm*/

// input: cho người dùng nhập số ngày đã làm việc

//output: Tính ra số tiền lương in ra màn hình


document.getElementById('Tinh1').onclick = function () {
    var ngayLamviec = (document.getElementById('soNgaylamviec')).value * 1;
    var luongMotngay = (document.getElementById('luongMotngay')).value * 1;

    var tienLuong = ngayLamviec * luongMotngay;


    document.getElementById('ketQua1').innerText = tienLuong + 'VNĐ';
}

//step1: tạo giao diện nhập ngày làm việc với thẻ input, số tiền công 1 ngày có giá trị sẵn, vào nút buttom và kết quả.
//step2: tạo hàm với nút button gắn cho hàm tính trả kết quả
//step3: chức năng hàm: lấy giá trị số ngày nhân giá trị công 1 ngày tính ra tiền lương giá trị cho biến. Sau đó in kết quả ra màn hình.

// //kết thúc bài 1



/*bài 2: Tính giá trị trung bình của 5 số nhập vào*/

//input: cho nhập 5 số

//output: số trung bình của 5 số

document.getElementById('Tinh2').onclick = function () {
    var so1 = (document.getElementById('so1')).value * 1;
    var so2 = (document.getElementById('so2')).value * 1;
    var so3 = (document.getElementById('so3')).value * 1;
    var so4 = (document.getElementById('so4')).value * 1;
    var so5 = (document.getElementById('so5')).value * 1;

    var soTrungbinh = (so1 + so2 + so3 + so4 + so5) / 5;

    document.getElementById('ketQua2').innerText = soTrungbinh;
}

//step1: tạo thẻ input nhập 5 số (so1,so2,so3,so4,so5)
//step2: tạo hàm với nút button gắn cho hàm tính trả kết quả
//step3: tạo biến nhận giá trị các số được nhập. Tạo biến nhận giá trung bình 5 số chia 5(soTrungbinh).
//step4: in ra màn hình kết quả

//kết thúc bài 2



// /*bài 3: Quy đổi tiền
// viết chương trình cho người dùng nhập số tiền USD đổi sang số tiền VNĐ */

//input: nhập số tiền muốn đổi

//output: in ra màn hình số tiền đổi được với tỉ giá 23000

document.getElementById('Tinh3').onclick = function () {
    var tiGia = (document.getElementById('tiGia')).value*1;
    var soTienmuondoi = (document.getElementById('soTienmuondoi')).value*1;

    var tienVND = tiGia * soTienmuondoi;

    document.getElementById('ketQua3').innerText = tienVND + 'VNĐ';
}


//step1: tạo thẻ nhập số tiền USD, giá đổi tiền
//step2: tạo hàm với nút button gắn cho hàm tính
//step3: tạo biến nhận giá trị tiền số tiền muốn đổi, biến nhận tỉ giá đổi, sau đó tính ra số tiền đổi được và gán biến. 
//step4: in ra màn hình kết quả.
//end bài 3



/*bài 4: Tính diện tích và chu vi hình chữ nhật
cho chiều rộng, chiều dài hình chữ nhật, tính diện tích chu vi hình chữ nhật*/

//input: nhập chiều dài, chiều rộng hình chữ nhật


//output: in ra màn hình Chu vi, diện tích hình chữ nhật

document.getElementById('Tinh4').onclick = function () {
    var canh1 = (document.getElementById('canh1')).value*1;
    var canh2 = (document.getElementById('canh2')).value*1;

    var chuVi = (canh1 + canh2) * 2;
    var dienTich = canh2 * canh1;

    document.getElementById('ketQua4a').innerText = chuVi + 'đơn vị chiều dài';
    document.getElementById('ketQua4b').innerText = dienTich + 'đơn vị diện tích';
}



//Step1: tạo thẻ nhập biến hai cạnh của hình chữ nhật
//step2: tạo hàm với nút button gắn cho hàm tính trả kết quả
//Step3: tạo biến nhận giá trị của 2 cạnh đã nhập, tạo biến chu vi(chuVi), diện tích(dienTich) nhận giá trị theo công thức
//step4: in ra màn hình kết quả

//end bài 4



/*bài 5: tính tổng hai ký số
nhập vào số có hai chữ số, tính tổng hai chữ số đó */

//input: nhập số 2 chữ số cần tính

//output: in ra màn hình Tổng của hai chữ số


document.getElementById('Tinh5').onclick = function () {

    var soCantinh = (document.getElementById('soCantinh')).value*1;

    var tongHaichuso = (soCantinh - (soCantinh % 10)) / 10 + soCantinh % 10;


    document.getElementById('ketQua5').innerText = tongHaichuso;
}

//Step1: tạo biến nhận số có hai chữ số(soCantinh)
//step2: tạo hàm với nút button gắn cho hàm tính trả kết quả
//Step3: tạo biến nhận giá trị của số nhập vào, tạo biến nhận giá trị tổng hai chữ số (tongHaichuso) theo  công thức: số đơn vị là số dư của số cần tính chia hết cho 10, số hàng chục là kết quả chia hết của số cần tính trừ đi số dư chia cho 10
//step4: in ra màn hình kết quả

//end bài 5
